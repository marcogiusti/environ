from __future__ import annotations
from collections import ChainMap
from collections.abc import Iterator, ItemsView, Mapping, MutableMapping
from functools import partial
import os
import os.path
import re
from urllib.parse import ParseResult, parse_qs, urlparse
from typing import Any, Callable, TextIO


__version__ = "0.9.2"


# dotenv parsing works well, there is no need to reinvent the wheel. This
# is a python translation of that code. Give credit to the author
#
# Copyright (c) 2015, Scott Motte
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


LINE_ENDING = "\r\n?"
PATTERN = (
    r"(?:^|^)"  # start of variable
    r"\s*"  # space
    r"(?:export\s+)?"
    r"(?P<name>[\w.-]+)"  # var name
    r"(?:\s*=\s*?|:\s+?)"  # equal sign or colon
    r"(?P<value>"
    r"\s*'(?:\\'|[^'])*'|"  # single quotes
    r'\s*"(?:\\"|[^"])*"|'  # double quotes
    r"\s*`(?:\\`|[^`])*`|"  # backtick quotes
    r"[^#\r\n]+)?"  # value
    r"\s*"  # space
    r"(?:#.*)?"  # comment
    r"(?:$|$)"  # end of variable
)
SURROUNDING_QUOTES = r"""^(['"`])([\s\S]*)\1$"""
USD_DOLLAR = r"(?!(?<=\\))\$"
VARIABLE_PATTERN = (
    "(?P<group>"
    r"(?!(?<=\\))\$"  # only match dollar signs that are not escaped
    "{?"  # optional opening curly brace
    r"(?P<name>[\w]+)"  # match the variable name
    r"(?::-(?P<default>[^}\\]*))?"  # match an optional default value
    "}?"  # optional closing curly brace
    ")"
)


def dotenv_parse(string: str) -> dict[str, str]:
    env = {}
    # Convert line breaks to same format
    string = re.sub(LINE_ENDING, "\n", string, flags=re.MULTILINE)
    for match in re.finditer(PATTERN, string, re.MULTILINE):
        if match:
            name, value = match.group("name", "value")
            # Default undefined or null to empty string
            if value is None:
                value = ""
            # Remove whitespace
            value = value.strip()
            # Check if double quoted
            double_quoted = value and value[0] == '"'
            # Remove surrounding quotes
            value = re.sub(SURROUNDING_QUOTES, r"\2", value, flags=re.MULTILINE)
            # Expand newlines if double quoted
            if double_quoted:
                value = value.replace(r"\n", "\n")
                value = value.replace(r"\r", "\r")
            env[name] = value
    return env


def interpolate(environ: Mapping[str, str], value: str) -> str:
    usd_matches = list(re.finditer(USD_DOLLAR, value))
    if not usd_matches:
        return value
    last_dollar_sign = usd_matches[-1].span()[0]
    rigth_most_group = value[last_dollar_sign:]
    match = re.match(VARIABLE_PATTERN, rigth_most_group)
    if match is not None:
        group, name, default = match.group("group", "name", "default")
        if default is None:
            default = ""  # NOTSET?
        expanded = environ.get(name, default)
        part1 = value[: usd_matches[-1].start()]
        part3 = value[usd_matches[-1].start() + match.end() :]
        return interpolate(environ, f"{part1}{expanded}{part3}")

    return value


def escape(value: str) -> str:
    return value.replace(r"\\$", "$")


# End dotenv


SOURCE = "__SOURCE__"
NOTSET = object()


def _assert_all_values_string(dct: Mapping[str, str]) -> None:
    for name, value in dct.items():
        if not isinstance(value, str):
            raise AssertionError(f"Invalid type for variable {name}, {type(value)}")


class EnvironBase(Mapping[str, str]):
    environ: Mapping[str, str]

    def __call__(self, name: str, default: Any = NOTSET, **kwargs: Any) -> Any:
        return self.get(name, default, **kwargs)

    def get(self, name: str, default: Any = NOTSET) -> Any:
        try:
            return self[name]
        except KeyError:
            if default is NOTSET:
                raise
            return default

    def __getitem__(self, name: str) -> str:
        return self.environ[name]

    def __iter__(self) -> Iterator[str]:
        return iter(self.environ)

    def __len__(self) -> int:
        return len(self.environ)


class Environ(EnvironBase):
    environ: Mapping[str, str]

    def __init__(
        self, environ: MutableMapping[str, str] | None = None, *, debug: bool = False
    ):
        if environ is None:
            environ = os.environ
        if debug:
            _assert_all_values_string(environ)
        self._defaults = dict[str, str]()
        self._envs = ChainMap[str, str]()
        self._overrides = ChainMap[str, str]()
        self.environ = ChainMap[str, str](
            self._overrides, environ, self._envs, self._defaults
        )
        self.debug = debug

    def set_defaults(
        self, defaults: MutableMapping[str, str] | None = None, **kwargs: str
    ) -> None:
        if not defaults:
            defaults = kwargs
        elif kwargs:
            defaults.update(kwargs)
        if self.debug:
            _assert_all_values_string(defaults)
        self._defaults.update(defaults)

    def set_env(
        self,
        env: MutableMapping[str, str] | None = None,
        *,
        overwrite: bool = False,
        source: str | None = None,
        **kwargs: str,
    ) -> None:
        if not env:
            env = kwargs
        elif kwargs:
            env.update(kwargs)
        if source is not None:
            env[SOURCE] = source
        if self.debug:
            _assert_all_values_string(env)
        if overwrite:
            self._overrides.maps.insert(0, env)
        else:
            self._envs.maps.insert(0, env)

    def parse(self, string: str) -> MutableMapping[str, str]:
        return dotenv_parse(string)

    def read_env_fileobj(
        self, fileobj: TextIO, *, overwrite: bool = False, source: str | None = None
    ) -> None:
        content = fileobj.read()
        env = self.parse(content)
        if source is None:
            try:
                source = os.path.abspath(fileobj.name)
            except AttributeError:
                pass
        self.set_env(env, overwrite=overwrite, source=source)

    def read_env(self, filename: str, *, overwrite: bool = False) -> None:
        with open(filename) as fp:
            self.read_env_fileobj(fp, overwrite=overwrite)


class ExpandEnvironItemsView(ItemsView[str, str]):
    _mapping: ExpandEnviron

    def __iter__(self) -> Iterator[tuple[str, str]]:
        env = self._mapping
        for name, value in env.environ.items():
            yield name, env.expand(value)


class ExpandEnviron(EnvironBase):
    def __init__(self, environ: Mapping[str, str], *, debug: bool = False):
        if debug:
            _assert_all_values_string(environ)
        self.environ = environ
        self.debug = debug

    def expand(self, value: str) -> str:
        return escape(interpolate(self.environ, value))

    def __getitem__(self, name: str) -> str:
        value = self.environ[name]
        return self.expand(value)

    def items(self) -> ExpandEnvironItemsView:
        return ExpandEnvironItemsView(self)


def boolean(value: str) -> bool:
    BOOLEAN_TRUE_STRINGS = ("true", "on", "ok", "y", "yes", "1")
    return value.lower() in BOOLEAN_TRUE_STRINGS


CastFunc = Callable[[str], Any]
DatabaseConfig = dict[str, Any]
DatabaseParseFunc = Callable[[str, ParseResult], DatabaseConfig]

_database_schemes: dict[str, DatabaseParseFunc] = {}


def register_db_scheme(scheme: str, parse_func: DatabaseParseFunc) -> None:
    _database_schemes[scheme] = parse_func


def database_url(value: str) -> DatabaseConfig:
    url = urlparse(value)
    try:
        parse_func = _database_schemes[url.scheme]
    except KeyError:
        raise ValueError(f"Unknown scheme '{url.scheme}'")
    return parse_func(value, url)


def _get_options(url: ParseResult) -> dict[str, str]:
    parameters = parse_qs(url.query)
    options = {keyword: values[-1] for keyword, values in parameters.items()}
    return options


def database_url_sqlite(value: str, url: ParseResult) -> DatabaseConfig:
    if url.netloc == ":memory:" or (url.netloc == "" and url.path == ""):
        name = ":memory:"
    elif url.netloc:
        name = f"{url.netloc}{url.path}"
    else:
        name = url.path
    options = _get_options(url)

    config = {"ENGINE": "django.db.backends.sqlite3", "NAME": name, "OPTIONS": options}
    return config


def database_url_postgresql(value: str, url: ParseResult) -> DatabaseConfig:
    # userspec
    try:
        user_pass, hostname = url.netloc.split("@", 1)
    except ValueError:
        user_pass = ""
        hostname = url.netloc
    try:
        user, password = user_pass.split(":", 1)
    except ValueError:
        user = user_pass
        password = ""

    # hostspec
    if len(hostname.split(",")) > 1:
        host = hostname
        port = ""
    else:
        try:
            host, port = hostname.split(":")
        except ValueError:
            host = hostname
            port = ""

    dbname = url.path[1:]

    # paramspec
    options = _get_options(url)

    config = {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": dbname,
        "USER": user,
        "PASSWORD": password,
        "HOST": host,
        "PORT": port,
        "OPTIONS": options,
    }
    return config


def database_url_postgis(value: str, url: ParseResult) -> DatabaseConfig:
    config = database_url_postgresql(value, url)
    config["ENGINE"] = "django.contrib.gis.db.backends.postgis"
    return config


register_db_scheme("sqlite", database_url_sqlite)
register_db_scheme("sqlite3", database_url_sqlite)
register_db_scheme("postgresql", database_url_postgresql)
register_db_scheme("postgres", database_url_postgresql)
register_db_scheme("postgis", database_url_postgis)


class ConvertEnviron(EnvironBase):
    _default_converters: dict[str, CastFunc] = {
        "bool": boolean,
        "int": int,
        "float": float,
        "database_url": database_url,
    }

    def __init__(self, environ: Mapping[str, str], *, debug: bool = False):
        if debug:
            _assert_all_values_string(environ)
        self.environ = environ
        self.debug = debug
        self._custom_converters: dict[str, CastFunc] = {}
        self._converters = ChainMap[str, CastFunc](
            self._custom_converters, self._default_converters
        )

    def register_converter(self, name: str, func: CastFunc) -> None:
        self._custom_converters[name] = func

    def __getattr__(self, name: str) -> Callable:
        try:
            converter = self._converters[name]
        except KeyError:
            raise AttributeError(name)

        return partial(self.get, cast=converter)

    def get(
        self, name: str, default: Any = NOTSET, *, cast: CastFunc | None = None
    ) -> Any:
        try:
            value = self[name]
        except KeyError:
            if default is NOTSET:
                raise
            return default
        if cast is not None:
            value = cast(value)
        return value


def environ(expand: bool = False) -> ConvertEnviron:
    environ = Environ()
    if "DOTENV" in os.environ:
        environ.read_env(os.environ["DOTENV"])
    elif os.path.exists(".env"):
        environ.read_env(".env")
    if expand:
        return ConvertEnviron(ExpandEnviron(environ))
    else:
        return ConvertEnviron(environ)
