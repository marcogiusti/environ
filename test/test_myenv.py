from contextlib import contextmanager
from io import StringIO
import os
import os.path
from pathlib import Path
import re
import unittest
from unittest.mock import patch

from environ import (
    ConvertEnviron,
    Environ,
    ExpandEnviron,
    SOURCE,
    environ,
    database_url,
)


def parse_file(testcase, filename):
    def eq(name, value, msg=None):
        testcase.assertIn(name, mapping, msg=msg)
        testcase.assertEqual(mapping[name], value, msg=msg)

    def missing(member, msg=None):
        testcase.assertNotIn(member, container=mapping, msg=msg)

    env = Environ()
    with Path(__file__).parent.joinpath(filename).open() as fp:
        mapping = env.parse(fp.read())
    return eq, missing


class TestParse(unittest.TestCase):
    def test_dotenv(self):
        eq, missing = parse_file(self, "dotenv.env")

        eq("BASIC", "basic", "sets basic environment variable")
        eq("AFTER_LINE", "after_line", "reads after a skipped line")
        eq("EMPTY", "", "defaults empty values to empty string")
        eq("EMPTY_SINGLE_QUOTES", "", "defaults empty values to empty string")
        eq("EMPTY_DOUBLE_QUOTES", "", "defaults empty values to empty string")
        eq("EMPTY_BACKTICKS", "", "defaults empty values to empty string")
        eq("SINGLE_QUOTES", "single_quotes", "escapes single quoted values")
        eq(
            "SINGLE_QUOTES_SPACED",
            "    single quotes    ",
            "respects surrounding spaces in single quotes",
        )
        eq("DOUBLE_QUOTES", "double_quotes", "escapes double quoted values")
        eq(
            "DOUBLE_QUOTES_SPACED",
            "    double quotes    ",
            "respects surrounding spaces in double quotes",
        )
        eq(
            "DOUBLE_QUOTES_INSIDE_SINGLE",
            'double "quotes" work inside single quotes',
            "respects double quotes inside single quotes",
        )
        eq(
            "DOUBLE_QUOTES_WITH_NO_SPACE_BRACKET",
            "{ port: $MONGOLAB_PORT}",
            "respects spacing for badly formed brackets",
        )
        eq(
            "SINGLE_QUOTES_INSIDE_DOUBLE",
            "single 'quotes' work inside double quotes",
            "respects single quotes inside double quotes",
        )
        eq(
            "BACKTICKS_INSIDE_SINGLE",
            "`backticks` work inside single quotes",
            "respects backticks inside single quotes",
        )
        eq(
            "BACKTICKS_INSIDE_DOUBLE",
            "`backticks` work inside double quotes",
            "respects backticks inside double quotes",
        )
        eq("BACKTICKS", "backticks")
        eq("BACKTICKS_SPACED", "    backticks    ")
        eq(
            "DOUBLE_QUOTES_INSIDE_BACKTICKS",
            'double "quotes" work inside backticks',
            "respects double quotes inside backticks",
        )
        eq(
            "SINGLE_QUOTES_INSIDE_BACKTICKS",
            "single 'quotes' work inside backticks",
            "respects single quotes inside backticks",
        )
        eq(
            "DOUBLE_AND_SINGLE_QUOTES_INSIDE_BACKTICKS",
            "double \"quotes\" and single 'quotes' work inside backticks",
            "respects single quotes inside backticks",
        )
        eq(
            "EXPAND_NEWLINES",
            "expand\nnew\nlines",
            "expands newlines but only if double quoted",
        )
        eq(
            "DONT_EXPAND_UNQUOTED",
            "dontexpand\\nnewlines",
            "expands newlines but only if double quoted",
        )
        eq(
            "DONT_EXPAND_SQUOTED",
            "dontexpand\\nnewlines",
            "expands newlines but only if double quoted",
        )
        missing("COMMENTS", msg="ignores commented lines")
        eq("INLINE_COMMENTS", "inline comments", "ignores inline comments")
        eq(
            "INLINE_COMMENTS_SINGLE_QUOTES",
            "inline comments outside of #singlequotes",
            "ignores inline comments and respects # character inside of single quotes",
        )
        eq(
            "INLINE_COMMENTS_DOUBLE_QUOTES",
            "inline comments outside of #doublequotes",
            "ignores inline comments and respects # character inside of double quotes",
        )
        eq(
            "INLINE_COMMENTS_BACKTICKS",
            "inline comments outside of #backticks",
            "ignores inline comments and respects # character inside of backticks",
        )
        eq(
            "INLINE_COMMENTS_SPACE",
            "inline comments start with a",
            "treats # character as start of comment",
        )
        eq("EQUAL_SIGNS", "equals==", "respects equals signs in values")
        eq("RETAIN_INNER_QUOTES", '{"foo": "bar"}', "retains inner quotes")
        eq("EQUAL_SIGNS", "equals==", "respects equals signs in values")
        eq("RETAIN_INNER_QUOTES", '{"foo": "bar"}', "retains inner quotes")
        eq("RETAIN_INNER_QUOTES_AS_STRING", '{"foo": "bar"}', "retains inner quotes")
        eq(
            "RETAIN_INNER_QUOTES_AS_BACKTICKS",
            '{"foo": "bar\'s"}',
            "retains inner quotes",
        )
        eq(
            "TRIM_SPACE_FROM_UNQUOTED",
            "some spaced out string",
            "retains spaces in string",
        )
        eq(
            "USERNAME",
            "therealnerdybeast@example.tld",
            "parses email addresses completely",
        )
        eq("SPACED_KEY", "parsed", "parses keys and values surrounded by spaces")

        mapping = Environ().parse("BUFFER=true")
        self.assertEqual(
            mapping["BUFFER"], "true", msg="should parse a buffer into an object"
        )
        expected = {"SERVER": "localhost", "PASSWORD": "password", "DB": "tests"}
        mapping = Environ().parse("SERVER=localhost\rPASSWORD=password\rDB=tests\r")
        self.assertEqual(mapping, expected, "can parse (\\r) line endings")
        mapping = Environ().parse("SERVER=localhost\nPASSWORD=password\nDB=tests\n")
        self.assertEqual(mapping, expected, "can parse (\\n) line endings")
        mapping = Environ().parse(
            "SERVER=localhost\r\nPASSWORD=password\r\nDB=tests\r\n"
        )
        self.assertEqual(mapping, expected, "can parse (\\r\\n) line endings")

    def test_dotenv_multiline(self):
        eq, missing = parse_file(self, "dotenv-multiline.env")

        eq("BASIC", "basic", "sets basic environment variable")
        eq("AFTER_LINE", "after_line", "reads after a skipped line")
        eq("EMPTY", "", "defaults empty values to empty string")
        eq("SINGLE_QUOTES", "single_quotes", "escapes single quoted values")
        eq(
            "SINGLE_QUOTES_SPACED",
            "    single quotes    ",
            "respects surrounding spaces in single quotes",
        )
        eq("DOUBLE_QUOTES", "double_quotes", "escapes double quoted values")
        eq(
            "DOUBLE_QUOTES_SPACED",
            "    double quotes    ",
            "respects surrounding spaces in double quotes",
        )
        eq(
            "EXPAND_NEWLINES",
            "expand\nnew\nlines",
            "expands newlines but only if double quoted",
        )
        eq(
            "DONT_EXPAND_UNQUOTED",
            "dontexpand\\nnewlines",
            "expands newlines but only if double quoted",
        )
        eq(
            "DONT_EXPAND_SQUOTED",
            "dontexpand\\nnewlines",
            "expands newlines but only if double quoted",
        )
        missing("COMMENTS", "ignores commented lines")
        eq("EQUAL_SIGNS", "equals==", "respects equals signs in values")
        eq("RETAIN_INNER_QUOTES", '{"foo": "bar"}', "retains inner quotes")
        eq("RETAIN_INNER_QUOTES_AS_STRING", '{"foo": "bar"}', "retains inner quotes")
        eq(
            "TRIM_SPACE_FROM_UNQUOTED",
            "some spaced out string",
            "retains spaces in string",
        )
        eq(
            "USERNAME",
            "therealnerdybeast@example.tld",
            "parses email addresses completely",
        )
        eq("SPACED_KEY", "parsed", "parses keys and values surrounded by spaces")
        eq(
            "MULTI_DOUBLE_QUOTED",
            "THIS\nIS\nA\nMULTILINE\nSTRING",
            "parses multi-line strings when using double quotes",
        )
        eq(
            "MULTI_SINGLE_QUOTED",
            "THIS\nIS\nA\nMULTILINE\nSTRING",
            "parses multi-line strings when using single quotes",
        )
        eq(
            "MULTI_BACKTICKED",
            'THIS\nIS\nA\n"MULTILINE\'S"\nSTRING',
            "parses multi-line strings when using single quotes",
        )

        mapping = Environ().parse("BUFFER=true")
        self.assertEqual(
            mapping["BUFFER"], "true", "should parse a buffer into an object"
        )
        expected_data = {"SERVER": "localhost", "PASSWORD": "password", "DB": "tests"}
        mapping = Environ().parse("SERVER=localhost\rPASSWORD=password\rDB=tests\r")
        self.assertEqual(mapping, expected_data, "can parse (\\r) line endings")
        mapping = Environ().parse("SERVER=localhost\nPASSWORD=password\nDB=tests\n")
        self.assertEqual(mapping, expected_data, "can parse (\\n) line endings")
        mapping = Environ().parse(
            "SERVER=localhost\r\nPASSWORD=password\r\nDB=tests\r\n"
        )
        self.assertEqual(mapping, expected_data, "can parse (\\r\\n) line endings")

    def test_dotenv_expand(self):
        eq, missing = parse_file(self, "dotenv-expand.env")

        eq("NODE_ENV", "test")
        eq("BASIC", "basic")
        eq("BASIC_EXPAND", "$BASIC")
        eq("MACHINE", "machine_env")
        eq("MACHINE_EXPAND", "$MACHINE")
        eq("UNDEFINED_EXPAND", "$UNDEFINED_ENV_KEY")
        eq("ESCAPED_EXPAND", r"\$ESCAPED")
        eq("DEFINED_EXPAND_WITH_DEFAULT", "${MACHINE:-default}")
        eq(
            "DEFINED_EXPAND_WITH_DEFAULT_NESTED",
            "${MACHINE:-${UNDEFINED_ENV_KEY:-default}}",
        )
        eq(
            "UNDEFINED_EXPAND_WITH_DEFINED_NESTED",
            "${UNDEFINED_ENV_KEY:-${MACHINE:-default}}",
        )
        eq("UNDEFINED_EXPAND_WITH_DEFAULT", "${UNDEFINED_ENV_KEY:-default}")
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_NESTED",
            "${UNDEFINED_ENV_KEY:-${UNDEFINED_ENV_KEY_2:-default}}",
        )
        eq(
            "DEFINED_EXPAND_WITH_DEFAULT_NESTED_TWICE",
            "${UNDEFINED_ENV_KEY:-${MACHINE}${UNDEFINED_ENV_KEY_3:-default}}",
        )
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_NESTED_TWICE",
            "${UNDEFINED_ENV_KEY:-${UNDEFINED_ENV_KEY_2:-${UNDEFINED_ENV_KEY_3:-default}}}",
        )
        eq(
            "DEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS",
            "${MACHINE:-/default/path:with/colon}",
        )
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS",
            "${UNDEFINED_ENV_KEY:-/default/path:with/colon}",
        )
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS_NESTED",
            "${UNDEFINED_ENV_KEY:-${UNDEFINED_ENV_KEY_2:-/default/path:with/colon}}",
        )
        eq("MONGOLAB_DATABASE", "heroku_db")
        eq("MONGOLAB_USER", "username")
        eq("MONGOLAB_PASSWORD", "password")
        eq("MONGOLAB_DOMAIN", "abcd1234.mongolab.com")
        eq("MONGOLAB_PORT", "12345")
        eq(
            "MONGOLAB_URI",
            "mongodb://${MONGOLAB_USER}:${MONGOLAB_PASSWORD}@${MONGOLAB_DOMAIN}:${MONGOLAB_PORT}/${MONGOLAB_DATABASE}",
        )
        eq("MONGOLAB_USER_RECURSIVELY", "${MONGOLAB_USER}:${MONGOLAB_PASSWORD}")
        eq(
            "MONGOLAB_URI_RECURSIVELY",
            "mongodb://${MONGOLAB_USER_RECURSIVELY}@${MONGOLAB_DOMAIN}:${MONGOLAB_PORT}/${MONGOLAB_DATABASE}",
        )
        eq(
            "WITHOUT_CURLY_BRACES_URI",
            "mongodb://$MONGOLAB_USER:$MONGOLAB_PASSWORD@$MONGOLAB_DOMAIN:$MONGOLAB_PORT/$MONGOLAB_DATABASE",
        )
        eq("WITHOUT_CURLY_BRACES_USER_RECURSIVELY", "$MONGOLAB_USER:$MONGOLAB_PASSWORD")
        eq(
            "WITHOUT_CURLY_BRACES_URI_RECURSIVELY",
            "mongodb://$MONGOLAB_USER_RECURSIVELY@$MONGOLAB_DOMAIN:$MONGOLAB_PORT/$MONGOLAB_DATABASE",
        )
        eq(
            "WITHOUT_CURLY_BRACES_UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS",
            "$UNDEFINED_ENV_KEY:-/default/path:with/colon",
        )

    def test_django_environ(self):
        eq, missing = parse_file(self, "django-environ.env")

        eq("DICT_VAR", "foo=bar,test=on")
        eq(
            "DATABASE_MYSQL_URL",
            "mysql://bea6eb0:69772142@us-cdbr-east.cleardb.com/heroku_97681?reconnect=true",
        )
        eq(
            "DATABASE_MYSQL_CLOUDSQL_URL",
            "mysql://djuser:hidden-password@//cloudsql/arvore-codelab:us-central1:mysqlinstance/mydatabase",
        )
        eq("DATABASE_MYSQL_GIS_URL", "mysqlgis://user:password@127.0.0.1/some_database")
        eq("CACHE_URL", "memcache://127.0.0.1:11211")
        eq(
            "CACHE_REDIS",
            "rediscache://127.0.0.1:6379/1?client_class=django_redis.client.DefaultClient&password=secret",
        )
        eq("EMAIL_URL", "smtps://user@domain.com:password@smtp.example.com:587")
        eq("URL_VAR", "http://www.google.com/")
        eq("PATH_VAR", "/home/dev")
        eq("BOOL_TRUE_STRING_LIKE_INT", "1")
        eq("BOOL_TRUE_INT", "1")
        eq("BOOL_TRUE_STRING_LIKE_BOOL", "True")
        eq("BOOL_TRUE_STRING_1", "on")
        eq("BOOL_TRUE_STRING_2", "ok")
        eq("BOOL_TRUE_STRING_3", "yes")
        eq("BOOL_TRUE_STRING_4", "y")
        eq("BOOL_TRUE_STRING_5", "true")
        eq("BOOL_TRUE_BOOL", "True")
        eq("BOOL_FALSE_STRING_LIKE_INT", "0")
        eq("BOOL_FALSE_INT", "0")
        eq("BOOL_FALSE_STRING_LIKE_BOOL", "False")
        eq("BOOL_FALSE_BOOL", "False")
        eq("DATABASE_SQLITE_URL", "sqlite:////full/path/to/your/database/file.sqlite")
        eq("JSON_VAR", '{"three": 33.44, "two": 2, "one": "bar"}')
        eq(
            "DATABASE_URL",
            "postgres://uf07k1:wegauwhg@ec2-107-21-253-135.compute-1.amazonaws.com:5431/d8r82722",
        )
        eq("FLOAT_VAR", "33.3")
        eq("FLOAT_COMMA_VAR", "33,3")
        eq("FLOAT_STRANGE_VAR1", "123,420,333.3")
        eq("FLOAT_STRANGE_VAR2", "123.420.333,3")
        eq("FLOAT_NEGATIVE_VAR", "-1.0")
        eq("PROXIED_VAR", "$STR_VAR")
        eq("ESCAPED_VAR", r"\$baz")
        eq("EMPTY_LIST", "")
        eq("EMPTY_INT_VAR", "")
        eq("INT_VAR", "42")
        eq("STR_LIST_WITH_SPACES", "foo,  bar")
        eq("STR_VAR", "bar")
        eq("MULTILINE_STR_VAR", r"foo\nbar")
        eq("MULTILINE_QUOTED_STR_VAR", "---BEGIN---\r\n---END---")
        eq("MULTILINE_ESCAPED_STR_VAR", r"---BEGIN---\\n---END---")
        eq("INT_LIST", "42,33")
        eq("CYRILLIC_VAR", "фуубар")
        eq("INT_TUPLE", "(42,33)")
        eq("MIX_TUPLE", "(42,Test)")
        eq("DATABASE_ORACLE_TNS_URL", "oracle://user:password@sid")
        eq("DATABASE_ORACLE_URL", "oracle://user:password@host:1521/sid")
        eq(
            "DATABASE_REDSHIFT_URL",
            "redshift://user:password@examplecluster.abc123xyz789.us-west-2.redshift.amazonaws.com:5439/dev",
        )
        eq(
            "DATABASE_CUSTOM_BACKEND_URL",
            "custom.backend://user:password@example.com:5430/database",
        )
        eq(
            "SAML_ATTRIBUTE_MAPPING",
            "uid=username;mail=email;cn=first_name;sn=last_name;",
        )
        eq("EXPORTED_VAR", "exported var")
        eq("PREFIX_TEST", "foo")


def parse_file_expand(testcase, filename):
    def eq(name, expected, msg=None):
        value = env.get(name, NOTSET)
        testcase.assertNotEqual(value, NOTSET, msg=f"Variable {name} not set")
        testcase.assertEqual(value, expected, msg=msg)

    def missing(member):
        testcase.assertIs(
            env.get(member, NOTSET),
            NOTSET,
            msg=f"{member} is present in the environment",
        )

    NOTSET = "__NOT_SET__"
    env = Environ()
    env.read_env(Path(__file__).parent.joinpath(filename))
    env = ExpandEnviron(env)
    return eq, missing


class TestExpandEnviron(unittest.TestCase):
    def test_dotenv_expand(self):
        eq, missing = parse_file_expand(self, "dotenv-expand.env")

        eq("NODE_ENV", "test")
        eq("BASIC", "basic")
        eq("BASIC_EXPAND", "basic")
        eq("MACHINE", "machine_env")
        eq("MACHINE_EXPAND", "machine_env")
        eq("UNDEFINED_EXPAND", "")
        eq("ESCAPED_EXPAND", r"\$ESCAPED")
        eq("DEFINED_EXPAND_WITH_DEFAULT", "machine_env")
        eq("DEFINED_EXPAND_WITH_DEFAULT_NESTED", "machine_env")
        eq("UNDEFINED_EXPAND_WITH_DEFINED_NESTED", "machine_env")
        eq("UNDEFINED_EXPAND_WITH_DEFAULT", "default")
        eq("UNDEFINED_EXPAND_WITH_DEFAULT_NESTED", "default")
        eq("DEFINED_EXPAND_WITH_DEFAULT_NESTED_TWICE", "machine_envdefault")
        eq("UNDEFINED_EXPAND_WITH_DEFAULT_NESTED_TWICE", "default")
        eq("DEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS", "machine_env")
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS",
            "/default/path:with/colon",
        )
        eq(
            "UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS_NESTED",
            "/default/path:with/colon",
        )
        eq("MONGOLAB_DATABASE", "heroku_db")
        eq("MONGOLAB_USER", "username")
        eq("MONGOLAB_PASSWORD", "password")
        eq("MONGOLAB_DOMAIN", "abcd1234.mongolab.com")
        eq("MONGOLAB_PORT", "12345")
        eq(
            "MONGOLAB_URI",
            "mongodb://username:password@abcd1234.mongolab.com:12345/heroku_db",
        )
        eq("MONGOLAB_USER_RECURSIVELY", "username:password")
        eq(
            "MONGOLAB_URI_RECURSIVELY",
            "mongodb://username:password@abcd1234.mongolab.com:12345/heroku_db",
        )
        eq(
            "WITHOUT_CURLY_BRACES_URI",
            "mongodb://username:password@abcd1234.mongolab.com:12345/heroku_db",
        )
        eq("WITHOUT_CURLY_BRACES_USER_RECURSIVELY", "username:password")
        eq(
            "WITHOUT_CURLY_BRACES_URI_RECURSIVELY",
            "mongodb://username:password@abcd1234.mongolab.com:12345/heroku_db",
        )
        eq(
            "WITHOUT_CURLY_BRACES_UNDEFINED_EXPAND_WITH_DEFAULT_WITH_SPECIAL_CHARACTERS",
            "/default/path:with/colon",
        )

    def test_expand_machine_variables(self):
        env = Environ({"MACHINE": "machine"})
        env.set_env(
            {"MACHINE_EXPAND": "${MACHINE}", "MACHINE_EXPAND_SIMPLE": "$MACHINE"}
        )
        env = ExpandEnviron(env)
        self.assertEqual(env["MACHINE_EXPAND"], "machine")
        self.assertEqual(env["MACHINE_EXPAND_SIMPLE"], "machine")

    def test_expand_prioritize_machine_variables(self):
        env = Environ({"MACHINE": "machine"})
        env.set_env(
            {
                "MACHINE": "machine_env",
                "MACHINE_EXPAND": "$MACHINE",
            }
        )
        env = ExpandEnviron(env)
        self.assertEqual(env["MACHINE_EXPAND"], "machine")

    def test_inline_expand(self):
        env = Environ()
        env.set_env({"INLINE_ESCAPED_EXPAND": r"pa\\$\\$word"})
        env = ExpandEnviron(env)
        self.assertEqual(env["INLINE_ESCAPED_EXPAND"], "pa$$word")

    def test_mixed_values(self):
        env = Environ()
        env.set_env({"PARAM1": "42", "MIXED_VALUES": r"\\$this$PARAM1\\$is${PARAM1}"})
        env = ExpandEnviron(env)
        self.assertEqual(env["MIXED_VALUES"], "$this42$is42")


def path(value):
    return value.split(":")


def int_list(value):
    return [int(v) for v in value.split(",")]


class TestConvertEnviron(unittest.TestCase):
    def test_work_with_plain_dict(self):
        env = ConvertEnviron({"FOO": "1"})
        self.assertEqual(env.get("FOO"), "1")
        self.assertEqual(env.get("FOO", cast=int), 1)
        self.assertEqual(env.int("FOO"), 1)

    def test_int(self):
        env = ConvertEnviron(Environ({"INT_VAR": "42"}))
        self.assertEqual(env.get("INT_VAR", cast=int), 42)
        self.assertEqual(env.int("INT_VAR"), 42)
        self.assertIsNone(env.get("NOT_PRESENT_VAR", cast=int, default=None))
        self.assertIsNone(env.get("EMPTY_INT_VAR", cast=int, default=None))

    def test_float(self):
        env = ConvertEnviron(
            Environ(
                {
                    "FLOAT_VAR": "33.3",
                    "FLOAT_NEGATIVE_VAR": "-1.0",
                    "FLOAT_COMMA_VAR": "33,3",
                    "FLOAT_STRANGE_VAR1": "123,420,333.3",
                    "FLOAT_STRANGE_VAR2": "123.420.333,3",
                }
            )
        )
        self.assertAlmostEqual(env.get("FLOAT_VAR", cast=float), 33.3)
        self.assertAlmostEqual(env.float("FLOAT_VAR"), 33.3)
        self.assertAlmostEqual(env.get("FLOAT_NEGATIVE_VAR", cast=float), -1.0)
        self.assertAlmostEqual(env.float("FLOAT_NEGATIVE_VAR"), -1.0)

        def tollerant_float(value):
            parts = re.split(r"[,.]", value)
            if len(parts) > 1:
                integer_part = "".join(parts[:-1])
                decimal_part = parts[-1]
                value = f"{integer_part}.{decimal_part}"
            return float(value)

        self.assertAlmostEqual(env.get("FLOAT_COMMA_VAR", cast=tollerant_float), 33.3)
        self.assertAlmostEqual(
            env.get("FLOAT_STRANGE_VAR1", cast=tollerant_float), 123420333.3
        )
        self.assertAlmostEqual(
            env.get("FLOAT_STRANGE_VAR2", cast=tollerant_float), 123420333.3
        )

    def test_boolean(self):
        env = ConvertEnviron(
            Environ(
                {
                    "BOOL_TRUE_STRING_LIKE_INT": "1",
                    "BOOL_TRUE_INT": "1",
                    "BOOL_TRUE_STRING_LIKE_BOOL": "True",
                    "BOOL_TRUE_STRING_1": "on",
                    "BOOL_TRUE_STRING_2": "ok",
                    "BOOL_TRUE_STRING_3": "yes",
                    "BOOL_TRUE_STRING_4": "y",
                    "BOOL_TRUE_STRING_5": "true",
                    "BOOL_TRUE_BOOL": "True",
                    "BOOL_FALSE_STRING_LIKE_INT": "0",
                    "BOOL_FALSE_INT": "0",
                    "BOOL_FALSE_STRING_LIKE_BOOL": "False",
                    "BOOL_FALSE_BOOL": "False",
                }
            )
        )
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_LIKE_INT"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_LIKE_BOOL"))
        self.assertIs(True, env.bool("BOOL_TRUE_INT"))
        self.assertIs(True, env.bool("BOOL_TRUE_BOOL"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_1"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_2"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_3"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_4"))
        self.assertIs(True, env.bool("BOOL_TRUE_STRING_5"))
        self.assertIs(False, env.bool("BOOL_FALSE_STRING_LIKE_INT"))
        self.assertIs(False, env.bool("BOOL_FALSE_INT"))
        self.assertIs(False, env.bool("BOOL_FALSE_STRING_LIKE_BOOL"))
        self.assertIs(False, env.bool("BOOL_FALSE_BOOL"))

    def test_list_of_ints(self):
        env = ConvertEnviron(Environ({"INT_LIST": "42,33"}))
        self.assertEqual(env.get("INT_LIST", cast=int_list), [42, 33])
        env.register_converter("int_list", int_list)
        self.assertEqual(env.int_list("INT_LIST"), [42, 33])

    def test_basic(self):
        env = Environ({})
        env.set_env(
            {
                "BOOL": "true",
                "INT": "42",
                "FLOAT": "4.2",
                "PATH": "/default/path:with/colon",
            }
        )
        env = ConvertEnviron(env)
        self.assertEqual(env.bool("BOOL"), True)
        self.assertEqual(env.int("INT"), 42)
        self.assertEqual(env.float("FLOAT"), 4.2)
        self.assertEqual(env.get("PATH", cast=path), ["/default/path", "with/colon"])
        env.register_converter("path_alias", path)
        self.assertEqual(env.path_alias("PATH"), ["/default/path", "with/colon"])

    def test_expand(self):
        environ = {
            "INT": "42",
            "PATH": "/default/path:with/colon",
            "EXPAND_INT": "$INT",
            "EXPAND_PATH": "$PATH:/some/other/path",
        }
        env = ConvertEnviron(ExpandEnviron(Environ(environ)))
        self.assertEqual(env.get("EXPAND_INT", cast=int), 42)
        self.assertEqual(
            env.get("EXPAND_PATH", cast=path),
            ["/default/path", "with/colon", "/some/other/path"],
        )


class TestDatabaseURL(unittest.TestCase):
    def test_database_url(self):
        with self.assertRaises(ValueError):
            database_url("mssql://hostname/dbname"),

    def test_postgresql(self):
        self.assertEqual(
            database_url("postgresql://"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "",
                "USER": "",
                "PASSWORD": "",
                "HOST": "",
                "PORT": "",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("postgresql://localhost"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "",
                "USER": "",
                "PASSWORD": "",
                "HOST": "localhost",
                "PORT": "",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("postgresql://localhost:5433"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "",
                "USER": "",
                "PASSWORD": "",
                "HOST": "localhost",
                "PORT": "5433",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("postgresql://localhost/mydb"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "mydb",
                "USER": "",
                "PASSWORD": "",
                "HOST": "localhost",
                "PORT": "",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("postgresql://user@localhost"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "",
                "USER": "user",
                "PASSWORD": "",
                "HOST": "localhost",
                "PORT": "",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("postgresql://user:secret@localhost"),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "",
                "USER": "user",
                "PASSWORD": "secret",
                "HOST": "localhost",
                "PORT": "",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url(
                "postgresql://other@localhost/otherdb?connect_timeout=10&application_name=myapp"
            ),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "otherdb",
                "USER": "other",
                "PASSWORD": "",
                "HOST": "localhost",
                "PORT": "",
                "OPTIONS": {
                    "connect_timeout": "10",
                    "application_name": "myapp",
                },
            },
        )
        self.assertEqual(
            database_url(
                "postgresql://host1:123,host2:456/somedb?target_session_attrs=any&application_name=myapp"
            ),
            {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "somedb",
                "USER": "",
                "PASSWORD": "",
                "HOST": "host1:123,host2:456",
                "PORT": "",
                "OPTIONS": {
                    "target_session_attrs": "any",
                    "application_name": "myapp",
                },
            },
        )

    def test_sqlite(self):
        self.assertEqual(
            database_url("sqlite://"),
            {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": ":memory:",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("sqlite://relative/path"),
            {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": "relative/path",
                "OPTIONS": {},
            },
        )
        self.assertEqual(
            database_url("sqlite:///absolute/path"),
            {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": "/absolute/path",
                "OPTIONS": {},
            },
        )


@contextmanager
def chdir(path):
    cwd = os.path.abspath(os.getcwd())
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(cwd)


class TestInterface(unittest.TestCase):
    def test_environ(self):
        environ = {}
        env = Environ(environ, debug=True)
        with self.assertRaises(AssertionError):
            env.set_env(FOO=1)
        env.set_defaults(FOO="1")
        self.assertEqual(env.get("FOO"), "1")
        self.assertEqual(env("FOO"), "1")
        env.set_env(FOO="2")
        self.assertEqual(env.get("FOO"), "2")
        environ["FOO"] = "3"
        self.assertEqual(env.get("FOO"), "3")
        env.set_env(FOO="4", overwrite=True)
        self.assertEqual(env.get("FOO"), "4")
        env.set_defaults({"BAR": "BAR"}, SPAM="SPAM")
        self.assertEqual(env.get("BAR"), "BAR")
        self.assertEqual(env.get("SPAM"), "SPAM")
        env.set_defaults(FOO="5")
        self.assertEqual(env.get("FOO"), "4")
        env.set_env(FOO="6")
        content = "BAR=2"
        env.read_env_fileobj(StringIO(content))
        self.assertEqual(env.get("BAR"), "2")
        with self.assertRaises(KeyError):
            env.get("SPAN")
        with self.assertRaises(KeyError):
            env["SPAN"]

    def test_expand(self):
        environ = {"FOO": "1", "BAR": "$FOO"}
        env = Environ(environ, debug=True)
        eenv = ExpandEnviron(env)

        self.assertEqual(eenv["FOO"], "1")
        self.assertEqual(eenv.get("FOO"), "1")
        self.assertEqual(eenv("FOO"), "1")
        self.assertEqual(eenv.get("BAR"), "1")
        self.assertEqual(eenv("BAR"), "1")
        with self.assertRaises(KeyError):
            eenv.get("SPAM")
        self.assertEqual(eenv.get("SPAM", "$FOO"), "$FOO")

    def test_convert(self):
        environ = {"FOO": "1", "BAR": "$FOO"}
        env = Environ(environ, debug=True)
        eenv = ExpandEnviron(env)
        cenv = ConvertEnviron(eenv)

        self.assertEqual(cenv["FOO"], "1")
        self.assertEqual(cenv.get("FOO"), "1")
        self.assertEqual(cenv.get("FOO", cast=int), 1)
        self.assertEqual(cenv("FOO"), "1")
        self.assertEqual(cenv("FOO", cast=int), 1)
        self.assertEqual(cenv.int("FOO"), 1)
        self.assertEqual(cenv.get("BAR"), "1")
        self.assertEqual(cenv.get("BAR", cast=int), 1)
        with self.assertRaises(KeyError):
            cenv.get("SPAM")
        self.assertEqual(cenv.get("SPAM", default=None), None)
        self.assertEqual(cenv.get("SPAM", cast=int, default=None), None)
        with self.assertRaises(AttributeError):
            cenv.bytes("FOO")

    @patch("os.environ", {})
    def test_environ_func(self):
        env = environ()
        self.assertEqual(dict(env.items()), {})
        my_env = Path(__file__).parent.joinpath("my_env")
        dotenv = my_env.joinpath("my.env")
        with patch("os.environ", {"DOTENV": str(dotenv)}):
            env = environ()
            self.assertEqual(
                dict(env.items()),
                {"DOTENV": str(dotenv), "FOO": "1", SOURCE: str(dotenv)},
            )
        with chdir(my_env):
            env = environ()
            self.assertEqual(
                dict(env.items()),
                {"FOO": "2", SOURCE: str(my_env.joinpath(".env"))},
            )
        with patch("os.environ", {"FOO": "1", "BAR": "$FOO"}):
            env = environ(expand=True)
            self.assertEqual(
                dict(env.items()),
                {"FOO": "1", "BAR": "1"},
            )

    def test_environ_base(self):
        env = Environ({"FOO": "1", "BAR": "2"})
        self.assertEqual(list(env), ["FOO", "BAR"])
        self.assertEqual(len(env), 2)
        self.assertEqual(list(env.keys()), ["FOO", "BAR"])
        self.assertEqual(dict(env.items()), {"FOO": "1", "BAR": "2"})
        env = ExpandEnviron(Environ({"FOO": "1", "BAR": "$FOO"}))
        self.assertEqual(dict(env.items()), {"FOO": "1", "BAR": "1"})
